package nl.utwente.di.translator;

public class Converter {

    public double getFahrenheit(String cels) {
        return (Double.parseDouble(cels) * 9/5) + 32;
    }
}